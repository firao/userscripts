// ==UserScript==
// @name        	Madokami Read Button
// @namespace       firao
// @version         1.0
// @namespace   	mdkm
// @description 	Always show read button on Madokami, regardless of screen width
// @include     	https://manga.madokami.al/*
// @grant       	GM_addStyle
// @updateURL       https://bitbucket.org/firao/userscripts/raw/master/madokami_read_button/madokami_read_button.meta.js
// @downloadURL     https://bitbucket.org/firao/userscripts/raw/master/madokami_read_button/madokami_read_button.user.js
// ==/UserScript==

GM_addStyle(".mobile-files-table tr :last-child { display: inline !important; }");