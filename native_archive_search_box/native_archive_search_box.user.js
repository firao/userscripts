// ==UserScript==
// @name          native archive search box
// @namespace     wew
// @version       1.0
// @author        -
// @grant         none
// @match         https://boards.4channel.org/*/archive
// @match         https://boards.4chan.org/*/archive
// @updateURL     https://bitbucket.org/firao/userscripts/raw/master/native_archive_search_box/native_archive_search_box.meta.js
// @downloadURL   https://bitbucket.org/firao/userscripts/raw/master/native_archive_search_box/native_archive_search_box.user.js
// ==/UserScript==

(function() {
  
  const searchBox = document.createElement("input");
  const searchDiv = document.createElement("div");
  searchDiv.textContent = "Search:";
  searchDiv.appendChild(searchBox);
  searchDiv.style = "padding-left: 10px";
  
  const table = document.getElementsByTagName("table")[0];
  table.previousElementSibling.previousElementSibling.prepend(searchDiv);
  
  searchBox.addEventListener("input", (event) => {
    
    const searchText = event.target.value.toLowerCase();
    const rows = document.getElementsByTagName("tr");
    
    for (let i = 1; i < rows.length; i++) {
      const textContent = rows[i].children[1].textContent.toLowerCase();

      if (textContent.indexOf(searchText) != -1 || searchText === "") {
        rows[i].style = "";
      } else {
        rows[i].style = "display:none";
      }
    }
  });
  
})();

