// ==UserScript==
// @name            native archive search box
// @namespace       wew
// @version         1.0
// @author          -
// @grant           none
// @match           https://boards.4channel.org/*/archive
// @match           https://boards.4chan.org/*/archive
// @updateURL       https://bitbucket.org/firao/userscripts/raw/master/native_archive_search_box/native_archive_search_box.meta.js
// @downloadURL     https://bitbucket.org/firao/userscripts/raw/master/native_archive_search_box/native_archive_search_box.user.js
// ==/UserScript==