// ==UserScript==
// @name        consolesave
// @namespace   cs
// @description add console.save(data, filename)
// @include     *
// @version     1.0
// @grant       none
// @updateURL   https://bitbucket.org/firao/userscripts/raw/master/consolesave/consolesave.meta.js
// @downloadURL https://bitbucket.org/firao/userscripts/raw/master/consolesave/consolesave.user.js
// ==/UserScript==