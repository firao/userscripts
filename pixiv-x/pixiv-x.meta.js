// ==UserScript==
// @name        	pixiv-x
// @namespace       firao
// @version         1.0
// @namespace   	pixiv-x
// @description 	Remove pixiv premium overlays
// @include     	https://www.pixiv.net/*
// @updateURL       https://bitbucket.org/firao/userscripts/raw/master/pixiv-x/pixiv-x.meta.js
// @downloadURL     https://bitbucket.org/firao/userscripts/raw/master/pixiv-x/pixiv-x.user.js
// ==/UserScript==