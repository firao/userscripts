// ==UserScript==
// @name            poe.trade account blacklist
// @namespace       firao
// @version         1.1
// @description     blacklisting feature on poe.trade
// @author          firao
// @grant           none
// @include         http://poe.trade/*
// @updateURL       https://bitbucket.org/firao/userscripts/raw/master/poe.trade_account_blacklist/poe.trade_account_blacklist.meta.js
// @downloadURL     https://bitbucket.org/firao/userscripts/raw/master/poe.trade_account_blacklist/poe.trade_account_blacklist.user.js
// ==/UserScript==