// ==UserScript==
// @name            poe.trade account blacklist
// @namespace       firao
// @version         1.1
// @description     blacklisting feature on poe.trade
// @author          firao
// @grant           none
// @include         http://poe.trade/*
// @updateURL       https://bitbucket.org/firao/userscripts/raw/master/poe.trade_account_blacklist/poe.trade_account_blacklist.meta.js
// @downloadURL     https://bitbucket.org/firao/userscripts/raw/master/poe.trade_account_blacklist/poe.trade_account_blacklist.user.js
// ==/UserScript==

(function() {
    var $ = jQuery;

    var thumbsUp        = "👍";
    var thumbsDown      = "👎";

    var blacklist = loadBlacklist();

    function displaySellerAccount() {
        var seller          = $(this).attr('data-seller');
        var href            = 'https://www.pathofexile.com/account/view-profile/' + seller;
        var cell            = $(this).find('.bottom-row .first-cell');
        var isBlacklisted   = blacklist.indexOf(seller.toLowerCase()) > -1;

        if (cell.find('.seller-username').length === 0) {

            // add seller acc name to cell
            var a1 = document.createElement('a');

            $(a1).text(seller)
                .attr("href", href)
                .attr("target", "_blank")
                .addClass("seller-username");

            // add blacklist button
            var a2 = document.createElement('a');

            $(a2).attr("href", "#")
                        .addClass("blacklist-button")
                        .bind("click", function() {
                            if (isBlacklisted) {
                                removeFromBlacklist(seller);
                            } else {
                                addToBlacklist(seller);
                            }
                            isBlacklisted = !isBlacklisted;
                            blacklistStyles(a1, a2, isBlacklisted);
                        });

            cell.append("[ ");
            cell.append(a1);
            cell.append(" | ");
            cell.append(a2);
            cell.append(" ]");

            blacklistStyles(a1, a2, isBlacklisted);
        }
    }

    function blacklistStyles(a1, a2, isBlacklisted) {
        if (isBlacklisted) {
            $(a1).css("color", "red");
            $(a2).text(thumbsDown);
        } else {
            $(a1).css("color", "green");
            $(a2).text(thumbsUp);
        }
    }

    function loadBlacklist() {
        if (localStorage["blacklist"] === undefined) {
            localStorage["blacklist"] = JSON.stringify([]);
        }
        return JSON.parse(localStorage["blacklist"]);
    }

    function saveBlacklist() {
        localStorage["blacklist"] = JSON.stringify(blacklist);
    }

    function addToBlacklist(seller) {
        var index = blacklist.indexOf(seller);
        if (index === -1) {
            blacklist.push(seller);
        }
        saveBlacklist();
    }

    function removeFromBlacklist(seller) {
        var index = blacklist.indexOf(seller);
        if (index !== -1) {
            blacklist.splice(index, 1);
        }
        saveBlacklist();
    }

    $('tbody.item').each(displaySellerAccount);

    $(document).bind("ajaxComplete", function () {
        $('tbody.item').each(displaySellerAccount);
    });

})();
